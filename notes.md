# Planned/Unused Music

https://www.fesliyanstudios.com/royalty-free-music

## Egyptian
Dehydrated.mp3
Afterlife.mp3

## Soft
Circle of Life.ogg // Test map
Mana Two - Part 2.mp3 // Test map
Atlantean Twilight.mp3
Edge of the World.mp3

## Jungle/Puzzle
https://www.fesliyanstudios.com/royalty-free-music/download/needle-in-a-haystack/2954

## Epic
https://www.fesliyanstudios.com/royalty-free-music/download/beyond-the-stars/1332

## Fire Zone 4
https://www.fesliyanstudios.com/royalty-free-music/download/escape-chase/340

## Acid
https://www.fesliyanstudios.com/royalty-free-music/download/needle-in-a-haystack/2954

## Lightning
Trial Core Scrap Brain Zone.mp3
https://www.fesliyanstudios.com/royalty-free-music/download/feels-good/417

## Shadow
https://www.fesliyanstudios.com/royalty-free-music/download/undercover-spy-agent/332
https://incompetech.com/music/royalty-free/mp3-royaltyfree/Shadowlands%201%20-%20Horizon.mp3

## Phase
https://incompetech.com/music/royalty-free/mp3-royaltyfree/Bump%20in%20the%20Night.mp3
https://incompetech.com/music/royalty-free/mp3-royaltyfree/Bent%20and%20Broken.mp3
https://www.fesliyanstudios.com/royalty-free-music/download/too-crazy/307
https://www.fesliyanstudios.com/royalty-free-music/download/ghost-stories/1291
https://incompetech.com/music/royalty-free/mp3-royaltyfree/Gloom%20Horizon.mp3

## Aether
Thunder Dreams.mp3
https://www.fesliyanstudios.com/royalty-free-music/download/tears-wont-stop/231
https://www.fesliyanstudios.com/royalty-free-music/download/the-hero-we-need/1860

## Nether

## Arcane
Fairytale Waltz.mp3
Glorious Morning.mp3?
Arcadia (incomp)

## Zenith Ward
The Ancient Ones.mp3
https://www.fesliyanstudios.com/royalty-free-music/download/emotion/458
https://www.fesliyanstudios.com/royalty-free-music/download/paranoia-dance/2909
https://incompetech.com/music/royalty-free/mp3-royaltyfree/SCP-x2x.mp3
https://incompetech.com/music/royalty-free/mp3-royaltyfree/Mana%20Two%20-%20Part%203.mp3

## Void
https://www.youtube.com/watch?v=FiSIgmq-Tn8
Monster in the Field.mp3

## Sources
https://www.youtube.com/@abdellahking3593/videos


# Vaultar Map Locations
Fire: Ambertide Peninsula - Maw - Unionica
Water: Isla Tranquila - Torrermelos - Ficonia
Earth: Chewhide Chasms - Goron - Unionica
Air:  Ollodar - Stratapar - Drahkul
Order: The Locked City - Estracia - Reteric
Chaos: Bedlam Nexus - Malgoth - Reteric
Acid: Duos - Morokai
Lava: Ellismar Volcano - Arahuela - Ficonia
Lightning: Fulgaro Treitz - Kanestreik - Gesia
Light: Imadom - Reteric
Shadow: Arozed - Reteric
Arbour: Guacanté - Ficonia
Frost: Glacies Pentaiz - Vonskii - Gesia
Fae: Collosus Spine - Nejichaeus - Drahkul
Poison: Fungal Creeps - Leotiva - Ficonia
Phase: Twilight Valleys - Veroquill - Drahkul
Arcane: Quadros - Morokai
Quake: Trembling Flats - Outcast - Unionica
Aether: Anomaly 7 - Pentos - Morokai
Nether: West Uncharted Shadowlands - Ofash - Unionica
Void: Neclego Nauzt - Balto - Gesia


## Heart Progression
Element Requirements

Frost - Fae, Lava
Poison - Fae, Frost
Fae - Arbour, Poison
Lava - Arbour, Poison
Arbour - Lava, Frost

Acid - Void, Quake, Aether - Pool vacuumes, extreme explosives, dynamis shielding.
Shadow - Acid, Nether, Light - Luminous acid pools, soulfire/demon-led pathways, light the way.
Quake - Shadow, Nether, Phase - Dark shock absorption, demon crusher jamming, phase through explosions/crushers.
Nether - Acid, Void, Lightning - Dissolve demons, rift consume demons, strike demons.
Void - Shadow, Quake, Arcane - Dark rift barriers, explosion feeding, reroute/move rifts.