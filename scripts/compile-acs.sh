#!/bin/bash
set -e

for file in acs/vaultar/*.acs; do
    acc $file -i ../schism-mythos
done
