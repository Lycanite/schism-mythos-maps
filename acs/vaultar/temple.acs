#include "zcommon.acs"
#import "acs/ncommon.acs"
#import "acs/nspeech.acs"
#import "acs/nclasses.acs"
#import "acs/ncinematic.acs"

str name = "Reteric Temple";
world int 1:templeLevelNum;
world bool 2:completedTrials[];
world bool 3:elementPowerups[];

script "Level Startup" OPEN {
    templeLevelNum = GetLevelInfo(LEVELINFO_LEVELNUM);

    // Scrolling Walls:
    Scroll_Wall(3, -8 << 16, -16 << 16, 0, 2);
    Scroll_Wall(4, 8 << 16, -16 << 16, 0, 2);

    PrintInfo(StrParam(s:name), "Alert/Signpost", true);
}

script "Level Update" REOPEN {
    templeLevelNum = GetLevelInfo(LEVELINFO_LEVELNUM);
    ACS_NamedExecute("Trial Checks", 0, 0, 0, 0);
}

script "Player Startup" ENTER {

}

script "Get Season" (void) {
    SetResultValue(1); // Summer
}

script "Goto Judgementa" (void) {
    ChangeLevel("JUDGEMEN", 1, 0);
}


// Music:
script "Music Passageways" (void) {
    LocalSetMusic("music/Temple of Horus.mp3");
}

script "Music Fountain" (void) {
    LocalSetMusic("music/Momia.mp3");
}

script "Music Heart" (void) {
    LocalSetMusic("music/Egypt.mp3");
}

script "Music Twisted Wing" (void) {
    LocalSetMusic("music/The Docks.mp3");
}

script "Music Proud Wing" (void) {
    LocalSetMusic("music/Earth.mp3");
}

script "Music Mesmulei" (void) {
    LocalSetMusic("music/The Ancient Ones.mp3");
}


// Shared:
str elementNames[28] = {
    "",
    "Fire",
    "Water",
    "Earth",
    "Air",
    "Order",
    "Chaos",
    "Acid",
    "Lava",
    "Lightning",
    "Light",
    "Shadow",
    "Arbour",
    "Frost",
    "Fae",
    "Poison",
    "Phase",
    "Arcane",
    "Quake",
    "Aether",
    "Nether",
    "Void",
    "Nova",
    "Flux",
    "Gravity",
    "Vortex",
    "Chrono",
    "Xeno"
};

str mapNames[28] = {
    "TEMPLE",
    "FIRE",
    "WATER",
    "EARTH",
    "AIR",
    "ORDER",
    "CHAOS",
    "ACID",
    "LAVA",
    "LIGHTNIN",
    "LIGHT",
    "SHADOW",
    "ARBOUR",
    "FROST",
    "FAE",
    "POISON",
    "PHASE",
    "ARCANE",
    "QUAKE",
    "AETHER",
    "NETHER",
    "VOID",
    "NOVA",
    "FLUX",
    "GRAVITY",
    "VORTEX",
    "CHRONO",
    "XENO"
};

int elementColors[28][3] = {
    { 255, 255, 255 }, // Unused 0 index
    { 255, 7, 3 }, // Fire
    { 24, 64, 197 }, // Water
    { 121, 95, 62 }, // Earth
    { 129, 199, 132 }, // Air
    { 255, 255, 0 }, // Order
    { 0, 87, 62 }, // Chaos
    { 118, 255, 3 }, // Acid
    { 255, 90, 0 }, // Lava
    { 0, 171, 245 }, // Lightning
    { 255, 238, 88 }, // Light
    { 21, 25, 35 }, // Shadow
    { 25, 87, 0 }, // Arbour
    { 129, 212, 250 }, // Frost
    { 236, 64, 122 }, // Fae
    { 182, 0, 214 }, // Poison
    { 0, 255, 210 }, // Phase
    { 82, 33, 181 }, // Arcane
    { 158, 158, 158 }, // Quake
    { 255, 167, 38 }, // Aether
    { 127, 0, 0 }, // Nether
    { 46, 1, 101 } // Void
};


// Maps:
script "Element Map" (int elementId, int spawnTag) {
    if (elementId < ELEMENT_FIRE || elementId > ELEMENT_XENO) {
        terminate;
    }
    ChangeLevel(mapNames[elementId], spawnTag, 0);
}


// Text:
script "Element Sign" (int elementId) {
    str status = completedTrials[elementId] ? " (Passed)" : "";
    PrintInfo(StrParam(s:"Trial of ", s:elementNames[elementId], s:status), "Alert/Signpost", false);
}

str zoneNames[5] = {
    "Temple Fountain",
    "Temple Heart",
    "Twisted Wing",
    "Proud Wing",
    "Mes'Mulei Gateway"
};
int lastZoneNameIdPerPlayer[256] = {};
script "Zone Sign" (int zoneId) {
    if (zoneId == lastZoneNameIdPerPlayer[PlayerNumber()] - 1) {
        terminate;
    }
    PrintInfo(zoneNames[zoneId], "Alert/Signpost", true);
    lastZoneNameIdPerPlayer[PlayerNumber()] = zoneId + 1;
}


// Temple Progression:
int stage = 0;
int newlyCompleted = -1;
int elementMarkerSectorTags[28] = { 0, 18, 19, 20, 21, 22, 23, 0, 30, 0, 0, 0, 28, 29, 27, 26 };

script "Debug" (void) {
    PrintInfo("Debug active!", "", true);
    stage = 5;
}

script "Complete Trial" (int elementId) {
    newlyCompleted = elementId;
    completedTrials[elementId] = true;
    AmbientSound("Alert/Complete", 127);

    // Light Up Marker:
    int markerSectorTag = elementMarkerSectorTags[elementId];
    if (markerSectorTag > 0) {
        Light_ChangeToValue(markerSectorTag, 255);
        Sector_SetColor(markerSectorTag, elementColors[elementId][0], elementColors[elementId][1], elementColors[elementId][2]);
    }
}

script "Trial Checks" (void) {
    int completedElementId = 0;
    if (newlyCompleted > 0) {
        completedElementId = newlyCompleted;
        newlyCompleted = 0;
    }
    Delay(2 * 35);
    if (completedElementId >= ELEMENT_FIRE && completedElementId <= ELEMENT_XENO) {
        Delay(PrintInfo(StrParam(s:"The Trial of ", s:elementNames[completedElementId], s:" has been completed!"), "", true));
    }

    // Fountain Stage:
    if (stage == 0) {
        if (!completedTrials[ELEMENT_FIRE] || !completedTrials[ELEMENT_WATER] || !completedTrials[ELEMENT_EARTH] || !completedTrials[ELEMENT_AIR] || !completedTrials[ELEMENT_ORDER] || !completedTrials[ELEMENT_CHAOS]) {
            PrintInfo("More trials await you around the Fountain...", "", true);
            terminate;
        }
        PrintInfo("Go north of the Fountain, the Heart awaits...", "", true);
        Floor_RaiseByValue(15, 1, 56);
        stage = 1;
        terminate;
    }

    // Heart Stage:
    if (stage == 1) {
        if (!completedTrials[ELEMENT_LAVA] || !completedTrials[ELEMENT_ARBOUR] || !completedTrials[ELEMENT_FROST] || !completedTrials[ELEMENT_FAE] || !completedTrials[ELEMENT_POISON]) {
            PrintInfo("More trials await you at the Heart...", "", true);
            terminate;
        }
        PrintInfo("Follow the red carpet south of the Fountain, the Twisted Wing awaits...", "", true);
        Floor_RaiseByValue(16, 1, 56);
        stage = 2;
        terminate;
    }

    // Twisted Wing Stage:
    if (stage == 2) {
        if (!completedTrials[ELEMENT_ACID] || !completedTrials[ELEMENT_SHADOW] || !completedTrials[ELEMENT_QUAKE] || !completedTrials[ELEMENT_NETHER] || !completedTrials[ELEMENT_VOID]) {
            PrintInfo("More trials await deep in the Twisted Wing...", "", true);
            terminate;
        }
        PrintInfo("Follow the red carpet south of the Fountain, the Proud Wing awaits...", "", true);
        Floor_RaiseByValue(17, 1, 56);
        stage = 3;
        terminate;
    }

    // Proud Wing Stage:
    if (stage == 3) {
        if (!completedTrials[ELEMENT_LIGHTNING] || !completedTrials[ELEMENT_LIGHT] || !completedTrials[ELEMENT_PHASE] || !completedTrials[ELEMENT_ARCANE] || !completedTrials[ELEMENT_AETHER]) {
            PrintInfo("More trials await up in the Proud Wing...", "", true);
            terminate;
        }
        PrintInfo("You have passed all of the trials... so far! The journey isn't over yet! The final chapter is coming soon!", "", true);
        terminate;
    }

    // Mes'Mulei Stage:
    if (stage == 4) {
        if (!completedTrials[ELEMENT_NOVA] || !completedTrials[ELEMENT_FLUX] || !completedTrials[ELEMENT_GRAVITY] || !completedTrials[ELEMENT_VORTEX] || !completedTrials[ELEMENT_CHRONO] || !completedTrials[ELEMENT_XENO]) {
            PrintInfo("More trials await at the Mes'Mulei Nexus...", "", true);
            terminate;
        }
        Floor_RaiseByValue(15, 1, 64);
        PrintInfo("The Zenith Elements are unleashed, return to Reteric Fountain...", "", true);
        terminate;
    }
}

script "Unlock Fountain" (void) {
    Floor_LowerByValue(5, 2, 56);
    Ceiling_RaiseByValue(5, 2, 56);
    Delay(2 * 35);

    Polyobj_Move(18, 2, 128, 56);
    Delay(2 * 35);

    Floor_LowerByValue(6, 2, 56);
    Ceiling_RaiseByValue(6, 2, 56);
    Delay(2 * 35);

    Polyobj_Move(20, 2, 128, 56);
    Delay(2 * 35);

    Floor_LowerByValue(7, 2, 56);
    Ceiling_RaiseByValue(7, 2, 56);
    Delay(2 * 35);

    Polyobj_Move(22, 2, 128, 56);
    Delay(2 * 35);

    Floor_LowerByValue(8, 2, 56);
    Ceiling_RaiseByValue(8, 2, 56);
    Delay(2 * 35);

    Polyobj_Move(24, 2, 128, 56);
    Delay(2 * 35);
}

bool heartUnlocked = false;
script "Unlock Heart" (void) {
    if (heartUnlocked) {
        SetResultValue(0);
        terminate;
    }
    if (stage < 1) {
        SetResultValue(0);
        PrintInfo("The switch is embedded too far into the ground and fails to activate.", "", false);
        terminate;
    }

    heartUnlocked = true;
    Floor_LowerToLowest(302, 32);
    TagWait(302);
    Stairs_BuildDownSync(303, 1, 16, 0);
}

bool wickedUnlocked = false;
script "Unlock Twisted Wing" (void) {
    if (wickedUnlocked) {
        SetResultValue(0);
        terminate;
    }
    if (stage < 2) {
        SetResultValue(0);
        PrintInfo("The switch is embedded too far into the ground and fails to activate.", "", false);
        terminate;
    }

    wickedUnlocked = true;
    Stairs_BuildUpSync(304, 1, 16, 0);
    TagWait(304);
    Door_Open(305, 2, 0);
}

bool proudUnlocked = false;
script "Unlock Proud Wing" (void) {
    if (proudUnlocked) {
        SetResultValue(0);
        terminate;
    }
    if (stage < 3) {
        SetResultValue(0);
        PrintInfo("The switch is embedded too far into the ground and fails to activate.", "", false);
        terminate;
    }

    proudUnlocked = true;
    Stairs_BuildUpSync(306, 1, 16, 0);
    TagWait(306);
    Door_Open(307, 2, 0);
}