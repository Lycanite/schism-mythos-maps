#include "zcommon.acs"
#import "acs/ncommon.acs"
#import "acs/nspeech.acs"
#import "acs/nclasses.acs"
#import "acs/ncinematic.acs"

world int 1:templeLevelNum;
world bool 2:completedTrials[];
world bool 3:elementPowerups[];

str name = "Trial of Earth";
int elementId = ELEMENT_EARTH;
bool complete = false;

script "Level Startup" OPEN {
    ACS_NamedExecuteWithResult("Level Refresh", 0, 0, 0, 0);
}

script "Level Update" REOPEN {
    ACS_NamedExecuteWithResult("Level Refresh", 0, 0, 0, 0);
}

script "Level Refresh" (void) {
    Delay(PrintInfo(!completedTrials[elementId] ? name : StrParam(s:name, s:" (Passed)"), "Alert/Signpost", true));
}

script "Player Startup" ENTER {

}

script "Get Season" (void) {
    SetResultValue(1); // Summer
}

script "Goto Temple" (void) {
    ChangeLevel("TEMPLE", elementId, 0);
}


// Global:
script "Complete Trial" (void) {
    completedTrials[elementId] = true;
    ACS_NamedExecuteAlways("Complete Trial", templeLevelNum, elementId, 0, 0);
    Delay(PrintInfo(StrParam(s:"You have passed the ", s:name, s:"!"), "", true));
    PrintInfo("Return to the Temple...", "", true);
}


// Signposts:
script "Signpost Cave" (void) {
    Delay(PrintInfo("Chewhide Chasms - Goron", "Alert/Signpost", false));
}

script "Signpost Metal" (void) {
    Delay(PrintInfo("Godforged Tower - Goron", "Alert/Signpost", false));
}

script "Signpost Dunes" (void) {
    Delay(PrintInfo("Cruskburrow Dunes - Goron", "Alert/Signpost", false));
}

script "Signpost Canyon" (void) {
    Delay(PrintInfo("Deathdrop Canyon - Goron", "Alert/Signpost", false));
}

script "Signpost Pyramid" (void) {
    Delay(PrintInfo("Ancient's Prism - Duazalor", "Alert/Signpost", false));
}


// Music:
script "Music Hub" (void) {
    LocalSetMusic("music/Tempting Secrets.mp3");
}

script "Music Cave" (void) {
    LocalSetMusic("music/Sands of Mystery.mp3");
}

script "Music Metal" (void) {
    LocalSetMusic("music/In The Desert.mp3");
}

script "Music Dunes" (void) {
    LocalSetMusic("music/Golden Age.mp3");
}

script "Music Canyon" (void) {
    LocalSetMusic("music/Full Moon Night.mp3");
}

script "Music Pyramid" (void) {
    LocalSetMusic("music/Return of the Mummy.mp3");
}


// Progress:
script "Complete Zone" (int lowerTag, int quakeThingTag) {
    Radius_Quake(4, 6 * 35, 0, 4, quakeThingTag);
    Floor_LowerToLowest(lowerTag, 16);
}


// Cave Zone:
script "Lower Stone Bridge" (void) {
    Radius_Quake(4, 3 * 35, 0, 4, 1);
    ACS_NamedExecute("Floor and Ceiling Lower To Lowest", 0, 25, 25, 128);
}


// Metal Zone:
int boxPolyNums[3] = { 11, 12, 13 };
int boxHeights[3] = { 128, 128, 256 };
str boxTextures[3] = { "textures/metal/MetalBox13.png", "textures/metal/MetalBox11.png", "textures/metal/MetalBox12.png" };
str boxFloorTexture = "textures/metal/MetalTile35.png";

/**
 * Slides a box poly from one node to another with a fallback if the target node is occupied.
 * Uses user_{direction}_node_tag expecting the closest node and then recusrviely checking that node and so on up to find the furthest accessible node.
 * @param originTag The tag of SchismNodeSpot to slide from.
 * @param direction The direction to slide in, checks the current node's adjacent nodes. 0 North, 1 East, 3 South, 4 West
*/
script "Slide Box" (int originTag, int direction) {
    int statusId = GetUserVariable(originTag, "user_status_id");
    if (statusId == 0) {
        terminate;
    }

    // Box:
    int boxId = statusid - 1;
    int polyNum = boxPolyNums[boxId];
    str boxTexture = boxTextures[boxId];
    int boxHeight = boxHeights[boxId];
    PolyWait(polyNum);

    // Origin Node:
    int originSectorTag = GetUserVariable(originTag, "user_sector_tag");
    int originLineTag = GetUserVariable(originTag, "user_line_tag");

    // Target Node:
    int targetTag = originTag;
    str directionNodeName = "user_north_node_tag";
    if (direction == 1) {
        directionNodeName = "user_east_node_tag";
    } else if (direction == 2) {
        directionNodeName = "user_south_node_tag";
    } else if (direction == 3) {
        directionNodeName = "user_west_node_tag";
    }
    int potentialTargetTag = originTag;
    while (potentialTargetTag > 0) { // Find next vacant node.
        potentialTargetTag = GetUserVariable(potentialTargetTag, directionNodeName);
        if (potentialTargetTag == 0) {
            break;
        }
        if (GetUserVariable(potentialTargetTag, "user_status_id") != 0) {
            break;
        }
        targetTag = potentialTargetTag;
    }
    if (targetTag == originTag) {
        terminate;
    }
    int targetSectorTag = GetUserVariable(targetTag, "user_sector_tag");
    int targetLineTag = GetUserVariable(targetTag, "user_line_tag");

    // Move From Origin:
    SetUserVariable(originTag, "user_status_id", 0);
    SetUserVariable(targetTag, "user_status_id", boxId + 1);
    Floor_LowerByValue(originSectorTag, boxHeight * 8, boxHeight);
    ChangeFloor(originSectorTag, boxFloorTexture);
    SetLineTexture(originLineTag, SIDE_FRONT, TEXTURE_BOTTOM, boxFloorTexture);
    Polyobj_MoveToSpot(polyNum, 64, targetTag);

    // Stop At Target:
    PolyWait(polyNum);
    Floor_RaiseByValue(targetSectorTag, boxHeight * 8, boxHeight);
    ChangeFloor(targetSectorTag, boxTexture);
    SetLineTexture(targetLineTag, SIDE_FRONT, TEXTURE_BOTTOM, boxTexture);
}


// Dunes Zone:
script "Dunes Zone" OPEN {
    Scroll_Floor(4, 0, -256, SCROLL_AND_CARRY);
    Scroll_Floor(5, -256, -256, SCROLL_AND_CARRY);
    Scroll_Floor(6, -256, 0, SCROLL_AND_CARRY);
    Scroll_Floor(7, -256, 256, SCROLL_AND_CARRY);
    Scroll_Floor(8, 0, 256, SCROLL_AND_CARRY);
    Scroll_Floor(9, 256, 256, SCROLL_AND_CARRY);
    Scroll_Floor(10, 256, 0, SCROLL_AND_CARRY);
    Scroll_Floor(11, 256, -256, SCROLL_AND_CARRY);
    Scroll_Ceiling(12, 250, 0, 0);
    Scroll_Wall(1, 0, -16 << 16, 0, 2);
    Scroll_Wall(1, 0, -16 << 16, 1, 2);
}